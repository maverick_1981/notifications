<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});



Route::get('/notify/{text}', function ($text) {
    
    /*
    echo Ably::time(); // 1467884220000
	$token = Ably::auth()->requestToken([ 'clientId' => 'client123', ]); // Ably\Models\TokenDetails
	Ably::channel('testChannel')->publish('testEvent', 'testPayload', '7qVgxw.Qk_NZg:MFDsHLZxo991gjRX');
	*/

	$ably = new Ably\AblyRest('7qVgxw.Qk_NZg:MFDsHLZxo991gjRX');
	$channel = $ably->channel('testChannel');
 
	// Publish a message to the test channel
	$channel->publish('myEvent', $text);

});




